

nums = [2,7,11,15]
target = 9

# nums = [3,2,4]
# target = 6

# nums = [3,3]
# target = 6


# nums = [3,3,5,7,5] 
# target = 8

def getIndexValuesForTargetNumber(nums, target):

    if not nums:
        return None
    
    index_values = [-1, -1]
    for idx1 in range(len(nums) - 1):
        missing_value = target - nums[idx1]
        if missing_value > 0:
            index_values[0] = idx1
            for idx2 in range(idx1 + 1, len(nums)):
                if missing_value == nums[idx2] :
                    index_values[1] = idx2
                    return index_values

    return index_values


print(getIndexValuesForTargetNumber(nums=nums, target=target))